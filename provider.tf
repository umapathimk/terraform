provider "aws" {
  region = var.region
  # Other AWS provider configuration options can be added here
  # e.g., credentials, profile, etc. as needed
}
