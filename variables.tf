variable "role_name" {
  description = "Name of the IAM role for NICE DCV licensing"
  type        = string
}

variable "policy_name" {
  description = "Name of the IAM policy for NICE DCV licensing"
  type        = string
}

variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"  # Update with your desired default region
}

variable "bucket_name" {
  description = "Name of the existing S3 bucket for NICE DCV licensing"
  type        = string
}
